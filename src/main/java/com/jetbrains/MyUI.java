package com.jetbrains;

import javax.servlet.annotation.WebServlet;

import clases.Lenguaje;
import clases.Programador;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.provider.BackEndDataProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;

import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        final VerticalLayout hLayout = new VerticalLayout();
        setContent( hLayout );
        hLayout.setResponsive( true );

        final TextField varNombre = new TextField();
        varNombre.setCaption("Nombres:");
        final TextField varApellidos = new TextField();
        varApellidos.setCaption("Apellidos:");
        final TextField varEdad = new TextField();
        varEdad.setCaption("Edad:");
        final TextField varNacionalidad = new TextField();
        varNacionalidad.setCaption("Nacionalidad:");
        final TextField varGenero = new TextField();
        varGenero.setCaption("Genero:");

        final TextField varLenguaje = new TextField();
        varLenguaje.setCaption("Lenguaje:");
        CheckBox checkB = new CheckBox("Es Compilado", true);

        // Crear Grid
        Grid<Lenguaje> gri = new Grid<>(Lenguaje.class);
        Lenguaje leng = new Lenguaje();

        Button buttonAddLenguaje = new Button("Agregar Lenguaje");

        buttonAddLenguaje.addClickListener( e -> {

            String lenguajes = varLenguaje.getValue();
            int esCompilado = Integer.parseInt(String.valueOf(checkB.getValue()));
            leng.setLenguaje( lenguajes );
            if( esCompilado > 0 )
                leng.setEsCompilado( true );

            gri.setItems( leng );
            hLayout.addComponent( gri );
            varLenguaje.setValue("");
        });

        // DEFINIR TITULO COLUMNAS
        Button button = new Button("Guardar Programador");

        button.addClickListener( e -> {
            hLayout.addComponent(new Label("Nombre: " + varNombre.getValue()
                    + " Apellidos: " + varApellidos.getValue() + " Nacionalidad: " + varNacionalidad.getValue()
                    + " Edad" + varEdad.getValue()   ));
        });


        hLayout.addComponents( varNombre, varApellidos, varEdad, varNacionalidad, varLenguaje, checkB, buttonAddLenguaje, gri, button );
        setContent( hLayout );

        setSizeFull();
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
